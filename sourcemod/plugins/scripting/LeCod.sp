#pragma semicolon 1
#define PLUGIN_VERSION "1.2"
#define PLUGIN_PREFIX "[\x06CoD:GO\x01]"
#define ZONE_MODEL "models/props/de_train/barrel.mdl"
#include <sourcemod>
#include <SDKTools>
#include <SDKHooks>
#include <cstrike>

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if (IsClientInGame( % 1))
	
#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if (IsClientInGame( % 1) && !IsFakeClient( % 1))
	
#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if (IsClientInGame( % 1) && IsPlayerAlive( % 1))
	
enum Precache {
	LaserMaterial, 
	HaloMaterial, 
	GlowSprite, 
}

Handle Models[Precache] = INVALID_HANDLE;

enum PlayerPassivePerks {
	dropExplosion, 
	hasSniper, 
};

enum PlayerActivePerks {
	flashRun, 
	lightRun, 
	doubleDamage, 
	stoppingDamage, 
	doubleRegen, 
};

enum GunPos {
	gunPrimary, 
	gunSecondary, 
};

ArrayList PerkZones;
int ZoneCount;
Handle g_ReloadZones = INVALID_HANDLE;
bool zone_Enabled[512] = true;
Handle g_pRegenHealth = INVALID_HANDLE;

int RedGlowSprite, BlueGlowSprite;

bool g_pPassivePerks[MAXPLAYERS + 1][PlayerPassivePerks];
bool g_pActivePerks[MAXPLAYERS + 1][PlayerActivePerks];
Handle g_pActivePerksReset[MAXPLAYERS + 1][PlayerActivePerks];

Handle g_GunAllowedSwitch[MAXPLAYERS + 1] = INVALID_HANDLE;
bool g_bGunAllowedSwitch[MAXPLAYERS + 1];
bool g_pOpenGunMenu[MAXPLAYERS + 1];

Handle p_RespawnMenu[MAXPLAYERS + 1];
float p_RespawnTime[MAXPLAYERS + 1];

bool p_notGunAllowed[MAXPLAYERS + 1];
char p_GunPreviousName[MAXPLAYERS + 1][512];
int p_GunPreviousClip[MAXPLAYERS + 1];
int p_GunPreviousReserve[MAXPLAYERS + 1];
int p_GunPreviousAmmoType[MAXPLAYERS + 1];

char p_CurrentLoadOut[MAXPLAYERS + 1][GunPos][512];

char heavyNames[][512] =  { "weapon_nova", "weapon_xm1014", "weapon_sawedoff", "weapon_mag7", "weapon_m249", "weapon_negev" };
char heavyRealNames[][512] =  { "Nova", "XM1014", "Sawed-Off", "Mag-7", "M249", "Negev" };

char smgsNames[][512] =  { "weapon_mac10", "weapon_mp7", "weapon_mp9", "weapon_bizon", "weapon_p90", "weapon_ump45" };
char smgsRealNames[][512] =  { "MAC-10", "MP7", "MP9", "PP-Bizon", "P90", "UMP-45" };

char riflesNames[][512] =  { "weapon_ak47", "weapon_aug", "weapon_famas", "weapon_galilar", "weapon_m4a1_silencer", "weapon_m4a1", "weapon_sg556" };
char riflesRealNames[][512] =  { "AK-47", "AUG", "FAMAS", "Galil AR", "M4A1-S", "M4A4", "SG 553" };

char pistolNames[][512] =  { "weapon_cz75a", "weapon_deagle", "weapon_elite", "weapon_fiveseven", "weapon_glock", "weapon_hkp2000", "weapon_p250", "weapon_revolver", "weapon_tec9", "weapon_usp_silencer" };
char pistolRealNames[][512] =  { "CZ-75", "Desert Eagle", "Dual Berettas", "Five-SeveN", "Glock-18", "P2000", "P250", "R8 Revolver", "Tec-9", "USP-S" };

char createTable1[] = "CREATE TABLE IF NOT EXISTS LeCod_Weapons (steamid varchar(32), primaryWeapon varchar(32), secondaryWeapon varchar(32), PRIMARY KEY (steamid));";
char insertTable1[] = "INSERT INTO LeCod_Weapons (steamid, primaryWeapon, secondaryWeapon) VALUES ('%s', '%s', '%s');";
char updatePrimaryTable1[] = "UPDATE LeCod_Weapons SET primaryWeapon = '%s' WHERE steamid = '%s';";
char updateSecondaryTable1[] = "UPDATE LeCod_Weapons SET secondaryWeapon = '%s' WHERE steamid = '%s';";
char selectTable1[] = "SELECT primaryWeapon, secondaryWeapon FROM LeCod_Weapons WHERE steamid = '%s';";

char createTable2[] = "CREATE TABLE IF NOT EXISTS LeCod_Points (mapname varchar(64), posx float, posy float, posz float, PRIMARY KEY (mapname, posx, posy, posz));";
char selectTable2[] = "SELECT posx, posy, posz FROM LeCod_Points WHERE mapname = '%s';";
char insertTable2[] = "INSERT INTO LeCod_Points (mapname, posx, posy, posz) VALUES ('%s', '%.2f', '%.2f', '%.2f');";

char createTable3[] = "CREATE TABLE IF NOT EXISTS LeCod_Playerstats (steamid varchar(64), kills int, teamwins int, mvps int, PRIMARY KEY (steamid));";
char insertTable3[] = "INSERT INTO LeCod_Playerstats (steamid, kills, teamwins, mvps) VALUES ('%s', '%i', '%i', '%i');";
char updateTable3Kills[] = "UPDATE LeCod_Playerstats SET kills = '%i' WHERE steamid = '%s';";
char updateTable3TeamWins[] = "UPDATE LeCod_Playerstats SET teamwins = '%i' WHERE steamid = '%s';";
char updateTable3Mvps[] = "UPDATE LeCod_Playerstats SET mvps = '%i' WHERE steamid = '%s';";
char selectTable3[] = "SELECT kills, teamwins, mvps FROM LeCod_Playerstats WHERE steamid = '%i';";
char deleteTable3[] = "DELETE FROM LeCod_Playerstats WHERE steamid = '%s';";

Handle DB;

public Plugin myinfo = 
{
	name = "LeCod", 
	author = "Oscar Wos (OSWO)", 
	description = "Cod Gamemode for CS:GO", 
	version = PLUGIN_VERSION, 
	url = "www.tangoworldwide.net", 
};

public void OnPluginStart()
{
	PerkZones = new ArrayList();
	
	char errormsg[512];
	DB = SQL_Connect("LeCod", false, errormsg, sizeof(errormsg));
	if (DB != INVALID_HANDLE)
	{
		DoSql();
	} else {
		CloseHandle(DB);
		PrintToServer("Error Connecting to DB! Error: %s", errormsg);
	}
	
	AddCommandListener(l_gunDrop, "drop");
	HookEvent("player_death", event_death);
	HookEvent("player_spawn", event_spawn);
	HookEvent("round_start", event_roundstart, EventHookMode_Post);
	HookEvent("game_start", event_gamestart, EventHookMode_Post);
	RegConsoleCmd("jointeam", Event_Join, "", 0);
	
	RegConsoleCmd("sm_info", command_info);
	RegConsoleCmd("sm_loadout", command_loadout);
	RegConsoleCmd("sm_test", command_test);
	RegConsoleCmd("sm_test2", command_test2);
	RegAdminCmd("sm_droptable", command_droptable, ADMFLAG_ROOT);
	
	LoopClients(i)
	{
		if (IsValidClient(i))
		{
			SDKHook(i, SDKHook_PostThink, sHook_PostThink);
			SDKHook(i, SDKHook_OnTakeDamage, sHook_OnTakeDamage);
			LoadClientData(i);
		}
	}
}

public Action Event_Join(client, args)
{
	if(IsValidClient(client))
	{
		char l_TeamArg[4];
		GetCmdArg(1, l_TeamArg, sizeof(l_TeamArg));
		int l_TeamNum = StringToInt(l_TeamArg);
		
		ChangeClientTeam(client, l_TeamNum);
		CS_RespawnPlayer(client);
	}
}

public Action event_roundstart(Event event, char[] name, bool dontBroadcast)
{
	for (int i = 0; i < ZoneCount; i++)
	{
		float pos[3];
		ArrayList temp = GetArrayCell(PerkZones, i);
		pos[0] = temp.Get(0);
		pos[1] = temp.Get(1);
		pos[2] = temp.Get(2);
		
		CreateTouchEntity(i, pos);
	}
}

public Action event_gamestart(Event event, char[] name, bool dontBroadcast)
{
	for (int i = 0; i < ZoneCount; i++)
	{
		float pos[3];
		ArrayList temp = GetArrayCell(PerkZones, i);
		pos[0] = temp.Get(0);
		pos[1] = temp.Get(1);
		pos[2] = temp.Get(2);
		
		CreateTouchEntity(i, pos);
	}
}

stock LoadClientData(int client)
{
	char steamID[32], buffer[512];
	GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
	
	Format(buffer, sizeof(buffer), selectTable1, steamID);
	SQL_TQuery(DB, sql_LoadInfoCallback, buffer, client);
}

public void sql_LoadInfoCallback(Handle owner, Handle hndl, const char[] error, int client)
{
	if (SQL_HasResultSet(hndl) && SQL_FetchRow(hndl))
	{
		char temp[32];
		SQL_FetchString(hndl, 0, temp, sizeof(temp));
		p_CurrentLoadOut[client][gunPrimary] = temp;
		
		SQL_FetchString(hndl, 1, temp, sizeof(temp));
		p_CurrentLoadOut[client][gunSecondary] = temp;
	}
}

public void DoSql()
{
	sql_CreateTables();
}

public void sql_CreateTables()
{
	Transaction createTables = SQL_CreateTransaction();
	
	SQL_AddQuery(createTables, createTable1);
	SQL_AddQuery(createTables, createTable2);
	SQL_AddQuery(createTables, createTable3);
	
	SQL_ExecuteTransaction(DB, createTables, tablesSuccess, tablesError);
}

public Action command_droptable(int client, int args)
{
	SQL_TQuery(DB, sql_DropCallback, "DROP TABLE LeCod_Points");
}

public void sql_DropCallback(Handle owner, Handle hndl, const char[] error, int client)
{
	
}

public void tablesSuccess(Handle db, any data, int numQueries, Handle[] results, any[] queryData)
{
	PrintToServer("Database tables succesfully created!");
}

public void tablesError(Handle db, any data, int numQueries, const char[] error, int failIndex, any[] queryData)
{
	PrintToServer("Error %s", error);
	SetFailState("Database tables could not be created! Error: %s", error);
}

public void OnMapStart()
{
	int entCount = GetMaxEntities();
	for (int i = 1; i < entCount; i++)
	{
		if (IsValidEntity(i))
		{
			char entName[512];
			GetEdictClassname(i, entName, sizeof(entName));
			
			if (StrEqual(entName, "func_buyzone"))
			{
				RemoveEdict(i);
			}
		}
	}
	ServerCommand("mp_death_drop_gun 0");
	ServerCommand("mp_death_drop_grenade 0");
	ServerCommand("mp_death_drop_defuser 0");
	ServerCommand("mp_ignore_round_win_conditions 1");
	ServerCommand("mp_do_warmup_period 0");
	ServerCommand("mp_roundtime 15");
	ServerCommand("mp_timelimit 15");
	
	for (int i = 2; i <= 3; i++)
	{
		SetTeamScore(i, 0);
		CS_SetTeamScore(i, 0);
	}
	
	RedGlowSprite = PrecacheModel("sprites/redglow1.vmt");
	BlueGlowSprite = PrecacheModel("sprites/blueglow1.vmt");
	PrecacheModel("models/props/de_train/barrel.mdl");
	
	Models[LaserMaterial] = PrecacheModel("materials/sprites/laserbeam.vmt");
	Models[HaloMaterial] = PrecacheModel("materials/sprites/glow01.vmt");
	Models[GlowSprite] = PrecacheModel("materials/sprites/blueflare1.vmt");
	
	LoadPoints();
	
	g_pRegenHealth = CreateTimer(0.2, regenHealth, _, TIMER_REPEAT);
	
	CreateTimer(5.0, restartGame);
}

public Action restartGame(Handle timer)
{
	ServerCommand("mp_restartgame 1");
}	

public Action regenHealth(Handle timer)
{
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			if (IsClientInGame(i) && IsPlayerAlive(i))
			{
				int curClientHealth = GetEntProp(i, Prop_Send, "m_iHealth");
				if (curClientHealth <= 99)
				{
					if (g_pActivePerks[i][doubleRegen])
					{
						SetEntProp(i, Prop_Send, "m_iHealth", curClientHealth + 2);
					} else {
						SetEntProp(i, Prop_Send, "m_iHealth", curClientHealth + 1);
					}
				}
			}
		}
	}
}

public void LoadPoints()
{
	char mapName[512], buffer[512];
	GetCurrentMap(mapName, sizeof(mapName));
	
	Format(buffer, sizeof(buffer), selectTable2, mapName);
	
	SQL_TQuery(DB, sql_LoadPointsCallback, buffer);
	
	g_ReloadZones = CreateTimer(1.0, reloadSprites, _, TIMER_REPEAT);
}

public Action reloadSprites(Handle timer)
{
	for (int i = 0; i < ZoneCount; i++)
	{
		float pos[3];
		
		ArrayList temp = PerkZones.Get(i);
		pos[0] = temp.Get(0);
		pos[1] = temp.Get(1);
		pos[2] = temp.Get(2);
		
		if (zone_Enabled[i])
			TE_SetupGlowSprite(pos, BlueGlowSprite, 1.0, 0.4, 249);
		else
			TE_SetupGlowSprite(pos, RedGlowSprite, 1.0, 0.4, 249);
		TE_SendToAll();
	}
}

public void sql_LoadPointsCallback(Handle owner, Handle hndl, const char[] error, int client)
{
	if (SQL_HasResultSet(hndl))
	{
		PerkZones.Clear();
		ZoneCount = 0;
		
		while (SQL_FetchRow(hndl))
		{
			float pos[3];
			pos[0] = SQL_FetchFloat(hndl, 0);
			pos[1] = SQL_FetchFloat(hndl, 1);
			pos[2] = SQL_FetchFloat(hndl, 2);
			
			ArrayList temp = CreateArray();
			temp.Push(pos[0]);
			temp.Push(pos[1]);
			temp.Push(pos[2]);
			
			CreateTouchEntity(ZoneCount, pos);
			zone_Enabled[ZoneCount] = true;
			
			PerkZones.Push(temp);
			ZoneCount++;
		}
	}
}

public void CreateTouchEntity(int zoneID, float pos[3])
{
	int iEnt = CreateEntityByName("trigger_multiple");
	if (iEnt > 0)
	{
		SetEntityModel(iEnt, ZONE_MODEL);
		
		if (IsValidEntity(iEnt))
		{
			DispatchKeyValue(iEnt, "spawnflags", "257");
			DispatchKeyValue(iEnt, "StartDisabled", "0");
			
			char buffer[512];
			Format(buffer, sizeof(buffer), "%i", zoneID);
			DispatchKeyValue(iEnt, "targetname", buffer);
			
			if (DispatchSpawn(iEnt))
			{
				ActivateEntity(iEnt);
				
				float tempx[3], tempy[3];
				tempx[0] = -10.0;
				tempx[1] = -10.0;
				tempx[2] = -50.0;
				
				tempy[0] = 10.0;
				tempy[1] = 10.0;
				tempy[2] = 50.0;
				
				SetEntPropVector(iEnt, Prop_Send, "m_vecMins", tempx);
				SetEntPropVector(iEnt, Prop_Send, "m_vecMaxs", tempy);
				SetEntProp(iEnt, Prop_Send, "m_nSolidType", 2);
				
				SetEntProp(iEnt, Prop_Send, "m_fEffects", GetEntProp(iEnt, Prop_Send, "m_fEffects") | 32);
				
				TeleportEntity(iEnt, pos, NULL_VECTOR, NULL_VECTOR);
				
				SDKHook(iEnt, SDKHook_StartTouch, StartTouchTrigger);
				//SDKHook(iEnt, SDKHook_EndTouch, EndTouchTrigger);				
			}
		}
	}
}

public Action StartTouchTrigger(int caller, int activator)
{
	if (activator < 1 || activator > MaxClients)
		return;
	
	char targetName[512];
	GetEntPropString(caller, Prop_Data, "m_iName", targetName, sizeof(targetName));
	
	int targetInt = StringToInt(targetName);
	if (zone_Enabled[targetInt])
	{
		zone_Enabled[targetInt] = false;
		
		CreateTimer(30.0, resetEnabled, targetInt);
		
		int temp = PickRandomPerk(activator);
		int y;
		
		for (int i = 0; i < 2; i++)
		{
			if (!g_pPassivePerks[activator][i])
				y++;
		}
		
		if (y == 0)
			temp = 1;
		
		if (ClientHasActivePerk(activator) && temp == 1)
		{
			int currentPerk = GetClientActivePerk(activator);
			if (currentPerk == 0)
				PrintToChat(activator, "%s You dropped \x10Flash Run\x01", PLUGIN_PREFIX);
			else if (currentPerk == 1)
				PrintToChat(activator, "%s You dropped \x10Light Run\x01", PLUGIN_PREFIX);
			else if (currentPerk == 2)
				PrintToChat(activator, "%s You dropped \x10Double Damage\x01", PLUGIN_PREFIX);
			else if (currentPerk == 3)
				PrintToChat(activator, "%s You dropped \x10Stopping Power\x01", PLUGIN_PREFIX);
			else if (currentPerk == 4)
				PrintToChat(activator, "%s You dropped \x10Double Regen\x01", PLUGIN_PREFIX);
			
			ResetActivePerks(activator);
			ResetSpeed(activator);
		}
		
		GiveRandomPerk(activator, temp);
	}
}

stock bool ClientHasActivePerk(int client)
{
	for (int i = 0; i < 5; i++)
	{
		if (g_pActivePerks[client][i])
			return true;
	}
	
	return false;
}

stock int GetClientActivePerk(int client)
{
	for (int i = 0; i < 5; i++)
	{
		if (g_pActivePerks[client][i])
			return i;
	}
	
	return -1;
}


public Action resetEnabled(Handle timer, int target)
{
	zone_Enabled[target] = true;
}

public Action EndTouchTrigger(int caller, int activator)
{
	PrintToChatAll("Test Activator: %i", activator);
}

public Action event_spawn(Event event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int x[10] =  { 0, 90, 90, 90, 90, 90, 30, 90, 90, 90 };
	
	for (int i = 1; i < 9; i++)
	{
		SetEntProp(client, Prop_Send, "m_iAmmo", x[i], _, i);
	}
	
	for (int i = 0; i < 5; i++)
	{
		int clientWeapon = GetPlayerWeaponSlot(client, i);
		if (clientWeapon != -1)
		{
			RemovePlayerItem(client, clientWeapon);
			RemoveEdict(clientWeapon);
		}
	}
	
	CreateTimer(0.01, GiveWeapons, client);
	
	g_bGunAllowedSwitch[client] = true;
	g_GunAllowedSwitch[client] = CreateTimer(10.0, resetSwitch, client);
}

public Action resetSwitch(Handle timer, int client)
{
	g_bGunAllowedSwitch[client] = false;
}

public Action GiveWeapons(Handle timer, int client)
{
	GivePlayerItem(client, p_CurrentLoadOut[client][gunPrimary]);
	GivePlayerItem(client, p_CurrentLoadOut[client][gunSecondary]);
	GivePlayerItem(client, "weapon_knife");
}

public void OnClientPutInServer(int client)
{
	char buffer[512], steamID[32], buffery[512];
	SDKHook(client, SDKHook_PostThink, sHook_PostThink);
	SDKHook(client, SDKHook_OnTakeDamage, sHook_OnTakeDamage);
	
	GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
	Format(buffery, sizeof(buffery), "SELECT weaponPrimary FROM LeCod_Weapons WHERE steamid = '%s'", steamID);
	
	if (!SQL_FastQuery(DB, buffery))
	{
		char tempx[32], tempy[32];
		Format(tempx, sizeof(tempx), "weapon_m4a1_silencer");
		Format(tempy, sizeof(tempy), "weapon_usp_silencer");
		
		Format(buffer, sizeof(buffer), insertTable1, steamID, tempx, tempy);
		SQL_TQuery(DB, sql_InsertTable1Callback, buffer, client);
	}
	
	LoadClientData(client);
	CS_RespawnPlayer(client);
}

public void sql_InsertTable1Callback(Handle owner, Handle hndl, const char[] error, int client)
{
	
}

public void OnClientDisconnect(int client)
{
	SDKHook(client, SDKHook_PostThink, sHook_PostThink);
	SDKUnhook(client, SDKHook_OnTakeDamage, sHook_OnTakeDamage);
	
	p_CurrentLoadOut[client][gunPrimary] = "";
	p_CurrentLoadOut[client][gunSecondary] = "";
	
	ClearHandles(client);
	
}

public void ClearHandles(int client)
{
	if (p_RespawnMenu[client] != INVALID_HANDLE)
	{
		CloseHandle(p_RespawnMenu[client]);
		p_RespawnMenu[client] = INVALID_HANDLE;
	}
}

public Action command_test(int client, int args)
{
	
}

public void sql_InsertPositionsCallback(Handle owner, Handle hndl, const char[] error, int client)
{
	
}

public void PlaceObject(int client)
{
	char buffer[512], mapName[512];
	float pos[3];
	GetClientAbsOrigin(client, pos);
	pos[2] = pos[2] + 50.0;
	
	GetCurrentMap(mapName, sizeof(mapName));
	Format(buffer, sizeof(buffer), insertTable2, mapName, pos[0], pos[1], pos[2]);
	SQL_TQuery(DB, sql_InsertPositionsCallback, buffer);
}

public Action command_loadout(int client, int args)
{
	ShowLoadoutMenu(client);
}

public void ShowLoadoutMenu(int client)
{
	if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
	{
		PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
		return;
	}
	
	g_pOpenGunMenu[client] = true;
	
	char buffer[512], clientName[128];
	GetClientName(client, clientName, sizeof(clientName));
	
	Menu menu = new Menu(LoadoutMenuHandle);
	Format(buffer, sizeof(buffer), "Current Loadout - %s\n", clientName);
	Format(buffer, sizeof(buffer), "%s \n", buffer);
	Format(buffer, sizeof(buffer), "%sPrimary Weapon - %s\n", buffer, p_CurrentLoadOut[client][gunPrimary]);
	Format(buffer, sizeof(buffer), "%sSecondary Weapon - %s\n", buffer, p_CurrentLoadOut[client][gunSecondary]);
	Format(buffer, sizeof(buffer), "%s \n", buffer);
	
	menu.SetTitle(buffer);
	
	Format(buffer, sizeof(buffer), "Change Primary!");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "Change Secondary!");
	menu.AddItem(buffer, buffer);
	
	menu.Display(client, 0);
}

public LoadoutMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		switch (option)
		{
			case 0:
			ChangePrimaryWeapon(client);
			case 1:
			ChangeSecondaryWeapon(client);
		}
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void ChangePrimaryWeapon(int client)
{
	char buffer[512];
	
	Menu menu = new Menu(PrimaryWeaponHandle);
	menu.SetTitle("Primary Weapon Groups");
	
	Format(buffer, sizeof(buffer), "Primary - Heavy");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "Primary - Smgs");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "Primary - Rifles");
	menu.AddItem(buffer, buffer);
	
	menu.Display(client, 0);
}

public PrimaryWeaponHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		switch (option)
		{
			case 0:
			ShowHeavyMenu(client);
			case 1:
			ShowSmgsMenu(client);
			case 2:
			ShowRiflesMenu(client);
		}
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void ShowHeavyMenu(int client)
{
	Menu menu = new Menu(HeavyMenuHandle);
	menu.SetTitle("Primary - Heavy");
	
	char buffer1[512], buffer2[512];
	
	for (int i = 0; i < 6; i++)
	{
		buffer1 = heavyNames[i];
		buffer2 = heavyRealNames[i];
		menu.AddItem(buffer1, buffer2);
	}
	
	menu.Display(client, 0);
}

public HeavyMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		char info[512], buffer[512], steamID[32];
		menu.GetItem(option, info, sizeof(info));
		
		GivePlayerNewWeapon(client, 0, info);
		ShowLoadoutMenu(client);
		
		GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
		Format(buffer, sizeof(buffer), updatePrimaryTable1, info, steamID);
		SQL_TQuery(DB, sql_UpdateTable1Callback, buffer);
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void ShowSmgsMenu(int client)
{
	Menu menu = new Menu(SmgsMenuHandle);
	menu.SetTitle("Primary - Smgs");
	
	char buffer1[512], buffer2[512];
	
	for (int i = 0; i < 6; i++)
	{
		buffer1 = smgsNames[i];
		buffer2 = smgsRealNames[i];
		menu.AddItem(buffer1, buffer2);
	}
	
	menu.Display(client, 0);
}

public SmgsMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		char info[512], buffer[512], steamID[32];
		menu.GetItem(option, info, sizeof(info));
		
		GivePlayerNewWeapon(client, 0, info);
		ShowLoadoutMenu(client);
		
		GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
		Format(buffer, sizeof(buffer), updatePrimaryTable1, info, steamID);
		SQL_TQuery(DB, sql_UpdateTable1Callback, buffer);
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void ShowRiflesMenu(int client)
{
	Menu menu = new Menu(RiflesMenuHandle);
	menu.SetTitle("Primary - Rifles");
	
	char buffer1[512], buffer2[512];
	
	for (int i = 0; i < 6; i++)
	{
		buffer1 = riflesNames[i];
		buffer2 = riflesRealNames[i];
		menu.AddItem(buffer1, buffer2);
	}
	
	menu.Display(client, 0);
}

public RiflesMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		char info[512], buffer[512], steamID[32];
		menu.GetItem(option, info, sizeof(info));
		
		GivePlayerNewWeapon(client, 0, info);
		ShowLoadoutMenu(client);
		
		GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
		Format(buffer, sizeof(buffer), updatePrimaryTable1, info, steamID);
		SQL_TQuery(DB, sql_UpdateTable1Callback, buffer);
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void ChangeSecondaryWeapon(int client)
{
	Menu menu = new Menu(SecondaryWeaponHandle);
	menu.SetTitle("Secondary Weapons");
	
	char buffer1[512], buffer2[512];
	
	for (int i = 0; i < 10; i++)
	{
		buffer1 = pistolNames[i];
		buffer2 = pistolRealNames[i];
		menu.AddItem(buffer1, buffer2);
	}
	
	menu.Display(client, 0);
}

public SecondaryWeaponHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_bGunAllowedSwitch[client] && IsPlayerAlive(client))
		{
			PrintToChat(client, "%s You may not switch your weapons now!", PLUGIN_PREFIX);
			return;
		}
		
		char info[512], buffer[512], steamID[32];
		menu.GetItem(option, info, sizeof(info));
		
		GivePlayerNewWeapon(client, 1, info);
		ShowLoadoutMenu(client);
		
		GetClientAuthId(client, AuthId_Engine, steamID, sizeof(steamID));
		Format(buffer, sizeof(buffer), updateSecondaryTable1, info, steamID);
		SQL_TQuery(DB, sql_UpdateTable1Callback, buffer);
	}
	
	if (option == MenuCancel_Exit)
		g_pOpenGunMenu[client] = false;
}

public void sql_UpdateTable1Callback(Handle owner, Handle hndl, const char[] error, int client)
{
	
}

public Action command_test2(int client, int args)
{
	PlaceObject(client);
}

public Action sHook_PostThink(int client)
{
	
}

public Action command_info(int client, int args)
{
	char buffer[512];
	Menu menu = new Menu(InfoMenuHandle);
	menu.SetTitle("COD Perks - Info");
	
	Format(buffer, sizeof(buffer), "Passive Perks Info");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "Active Perks Info");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "--------------------");
	menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
	
	Format(buffer, sizeof(buffer), "View Active Perks");
	menu.AddItem(buffer, buffer);
	
	menu.Display(client, 0);
}

public InfoMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		switch (option)
		{
			case 0: {
				DisplayPassiveInfo(client);
			}
			case 1: {
				DisplayActiveInfo(client);
			}
			case 3: {
				DisplayCurrentPerks(client, 0);
			}
		}
	}
}

public Action event_death(Event event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int killer = GetClientOfUserId(event.GetInt("attacker"));
	
	int killerTeam = GetClientTeam(killer);
	int currentScore = CS_GetTeamScore(killerTeam);
	currentScore++;
	
	SetTeamScore(killerTeam, currentScore);
	CS_SetTeamScore(killerTeam, currentScore);
	
	if (g_pPassivePerks[client][dropExplosion])
	{
		float deathPos[3];
		GetClientAbsOrigin(client, deathPos);
		DropNade(client, deathPos);
	}
	
	p_RespawnTime[client] = 5.0;
	p_RespawnMenu[client] = CreateTimer(0.1, respawnMenu, client, TIMER_REPEAT);
	
	if (currentScore == 100)
		MapEnd(killerTeam);
	
	ResetSpeed(client);
	ResetActivePerks(client);
	ResetPassivePerks(client);
	ResetPreviousWeapon(client);
}

public void MapEnd(int team)
{
	char buffer[512];
	switch (team)
	{
		case 0:
		Format(buffer, sizeof(buffer), "Invalid!");
		case 1:
		Format(buffer, sizeof(buffer), "Spec");
		case 2:
		Format(buffer, sizeof(buffer), "Terrorists");
		case 3:
		Format(buffer, sizeof(buffer), "Counter Terrorists");
	}
	
	PrintToChatAll("%s -------------------------", PLUGIN_PREFIX);
	PrintToChatAll("%s Team Won: %s", PLUGIN_PREFIX, buffer);
	
	if (g_ReloadZones != INVALID_HANDLE)
	{
		CloseHandle(g_ReloadZones);
		g_ReloadZones = INVALID_HANDLE;
	}
	
	CreateTimer(10.0, forceNextMap);
	
}

public Action forceNextMap(Handle timer)
{
	char nextMap[512];
	GetNextMap(nextMap, sizeof(nextMap));
	ForceChangeLevel(nextMap, "Limit Reached");
}

public Action respawnMenu(Handle timer, int client)
{
	if (p_RespawnTime[client] > 0.01)
		p_RespawnTime[client] = p_RespawnTime[client] - 0.1;
	
	if (!g_pOpenGunMenu[client])
	{
		char buffer[512], buffer1[512];
		
		Menu menu = new Menu(RespawnMenuHandle);
		
		Format(buffer1, sizeof(buffer1), "Click To Respawn!");
		
		if (p_RespawnTime[client] > 0.01)
		{
			Format(buffer, sizeof(buffer), "Current Respawn Time: %.2f", p_RespawnTime[client]);
			menu.AddItem(buffer1, buffer1, ITEMDRAW_DISABLED);
			
		} else {
			Format(buffer, sizeof(buffer), "Current Respawn Time: You Can Respawn Now!");
			menu.AddItem(buffer1, buffer1);
		}
		
		menu.SetTitle(buffer);
		menu.Display(client, 0);
	}
}

public RespawnMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		CS_RespawnPlayer(client);
		
		if (p_RespawnMenu[client] != INVALID_HANDLE)
		{
			CloseHandle(p_RespawnMenu[client]);
			p_RespawnMenu[client] = INVALID_HANDLE;
		}
	}
}

public void DropNade(int client, float pos[3])
{
	int iHe = CreateEntityByName("hegrenade_projectile");
	if ((iHe != -1) && DispatchSpawn(iHe))
	{
		SetEntPropFloat(iHe, Prop_Send, "m_flElasticity", 0.8);
		SetEntProp(iHe, Prop_Data, "m_nNextThinkTick", -1);
		
		TeleportEntity(iHe, pos, NULL_VECTOR, NULL_VECTOR);
		
		DataPack datapack = new DataPack();
		datapack.WriteCell(iHe);
		datapack.WriteCell(client);
		
		CreateTimer(2.0, Timer_NadeExplode, datapack, TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action Timer_NadeExplode(Handle timer, DataPack datapack)
{
	float pos[3];
	
	datapack.Reset();
	int iHe = datapack.ReadCell();
	int client = datapack.ReadCell();
	
	GetEntPropVector(iHe, Prop_Send, "m_vecOrigin", pos);
	SetEntProp(iHe, Prop_Data, "m_takedamage", 2);
	SetEntProp(iHe, Prop_Data, "m_iHealth", 1);
	
	SDKHooks_TakeDamage(iHe, 0, 0, 100.0);
	DropExplosion(client, pos);
}

public Action sHook_OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype)
{
	damage *= 0.5;
	
	if (IsValidEntity(inflictor))
	{
		if (g_pActivePerks[inflictor][doubleDamage])
			damage *= 2;
		
		if (g_pActivePerks[inflictor][stoppingDamage])
			damage *= 1.25;
		
		if (!IsPlayerAlive(inflictor))
		{
			if (IsValidClient(inflictor))
			{
				if (GetClientTeam(inflictor) != GetClientTeam(victim))
				{
					char attackerName[512];
					GetClientName(attacker, attackerName, sizeof(attackerName));
					
					PrintToChat(victim, "%s %s's explosion hit you for %.2f", PLUGIN_PREFIX, attackerName, damage);
				}
			}
		}
		
	}
	return Plugin_Changed;
}

public Action l_gunDrop(int client, char[] command, int argc)
{
	int weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	if (weapon != -1)
	{
		char weaponName[64];
		GetEdictClassname(weapon, weaponName, sizeof(weaponName));
		
		if (StrEqual(weaponName, "weapon_c4"))
			return Plugin_Continue;
		else
		{
			PrintHintText(client, "Dropping Guns is not allowed!");
			return Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
}

stock bool DropExplosion(int client, float pos[3])
{
	int explosion = CreateEntityByName("env_explosion");
	
	if (explosion != -1)
	{
		DispatchKeyValueVector(explosion, "Origin", pos);
		DispatchKeyValue(explosion, "iMagnitude", "200");
		DispatchKeyValue(explosion, "iRadiusOverride", "450.0");
		
		DispatchSpawn(explosion);
		
		if (client > 0 && IsValidClient(client) && IsClientInGame(client))
		{
			SetEntPropEnt(explosion, Prop_Send, "m_hOwnerEntity", client);
			SetEntPropEnt(explosion, Prop_Data, "m_hInflictor", client);
		}
		
		AcceptEntityInput(explosion, "Explode");
		AcceptEntityInput(explosion, "Kill");
		
		return (true);
	} else
		return (false);
}

stock bool IsValidClient(int client, bool alive = false)
{
	if (client >= 1 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client) && (alive == false || IsPlayerAlive(client)))
		return true;
	return false;
}

public int PickRandomPerk(int client)
{
	int x;
	
	x = GetRandomInt(0, 1);
	
	return x;
}

public void GiveRandomPerk(int client, int x)
{
	int returnVal;
	
	switch (x)
	{
		case 0:
		returnVal = RandomizePassivePerk(client);
		case 1:
		returnVal = RandomizeActivePerk();
	}
	
	GiveBenefit(client, x, returnVal);
}

stock int RandomizePassivePerk(int client)
{
	int randomArray[10], y;
	
	for (int i = 0; i < 2; i++)
	{
		if (!g_pPassivePerks[client][i])
			randomArray[y++] = i;
	}
	
	if (y == 0)
		return -1;
	
	int random = randomArray[GetRandomInt(0, y - 1)];
	return random;
}

stock int RandomizeActivePerk()
{
	int random = GetRandomInt(0, 4);
	return random;
}

stock bool GiveBenefit(int client, int primary, int secondary)
{
	switch (primary)
	{
		case 0:
		{
			switch (secondary)
			{
				case  - 1:
				{
					ResetActivePerks(client);
					int returnVal;
					returnVal = RandomizeActivePerk();
					
					GiveBenefit(client, 1, returnVal);
				}
				case 0:
				p_deathNade(client);
				case 1:
				p_hasSniper(client);
			}
		}
		case 1:
		{
			switch (secondary)
			{
				case 0:
				p_FlashRun(client);
				case 1:
				p_LightRun(client);
				case 2:
				p_DoubleDamage(client);
				case 3:
				p_StoppingDamage(client);
				case 4:
				p_DoubleRegen(client);
			}
		}
	}
}

stock ResetActivePerks(int client)
{
	for (int i = 0; i < 5; i++)
	{
		g_pActivePerks[client][i] = false;
		
		if (g_pActivePerksReset[client][i] != INVALID_HANDLE)
		{
			CloseHandle(g_pActivePerksReset[client][i]);
			g_pActivePerksReset[client][i] = INVALID_HANDLE;
		}
	}
}

stock ResetPassivePerks(int client)
{
	for (int i = 0; i < 2; i++)
	g_pPassivePerks[client][i] = false;
}

public void DisplayPassiveInfo(int client)
{
	
}

public void DisplayActiveInfo(int client)
{
	
}

public void DisplayCurrentPerks(int client, int itemLocation)
{
	char buffer[512];
	
	Menu menu = new Menu(DisplayCurrentHandle);
	menu.SetTitle("Current Perks");
	
	Format(buffer, sizeof(buffer), "Passive Perks");
	menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
	
	Format(buffer, sizeof(buffer), "--------------------\n");
	menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
	
	
	if (g_pPassivePerks[client][dropExplosion])
		Format(buffer, sizeof(buffer), "Death Nade - Active");
	else
		Format(buffer, sizeof(buffer), "Death Nade - Inactive");
	menu.AddItem(buffer, buffer);
	
	if (g_pPassivePerks[client][hasSniper])
		Format(buffer, sizeof(buffer), "Sniper Equipped - Active");
	else
		Format(buffer, sizeof(buffer), "Sniper Equipped - Inactive");
	menu.AddItem(buffer, buffer);
	
	menu.DisplayAt(client, itemLocation, 0);
}

public DisplayCurrentHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (option <= 6)
			DisplayCurrentPerks(client, 0);
		else
			DisplayCurrentPerks(client, 7);
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float pAngle[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (buttons & IN_USE)
	{
		if (g_pPassivePerks[client][hasSniper])
		{
			GiveNewWeapon(client);
		}
	}
}

public void GiveNewWeapon(int client)
{
	if (!p_notGunAllowed[client])
	{
		int slotWeapon0 = GetPlayerWeaponSlot(client, 0);
		
		if (slotWeapon0 != -1)
		{
			char currentWeapon[512];
			GetEntityClassname(slotWeapon0, currentWeapon, sizeof(currentWeapon));
			
			int currentClip = GetEntProp(slotWeapon0, Prop_Send, "m_iClip1");
			int currentAmmoType = GetEntProp(slotWeapon0, Prop_Send, "m_iPrimaryAmmoType");
			
			int currentReserve = GetEntProp(client, Prop_Send, "m_iAmmo", _, currentAmmoType);
			
			//PrintToChatAll("currentweap %s clip: %i, ammo: %i, reserve: %i", currentWeapon, currentClip, currentAmmoType, currentReserve);
			
			RemovePlayerItem(client, slotWeapon0);
			RemoveEdict(slotWeapon0);
			
			if (StrEqual(p_GunPreviousName[client], ""))
			{
				CreateGunAndAmmo(client, "weapon_awp", 1, 1, 6);
			} else {
				CreateGunAndAmmo(client, p_GunPreviousName[client], p_GunPreviousClip[client], p_GunPreviousReserve[client], p_GunPreviousAmmoType[client]);
			}
			
			p_GunPreviousName[client] = currentWeapon;
			p_GunPreviousClip[client] = currentClip;
			p_GunPreviousAmmoType[client] = currentAmmoType;
			p_GunPreviousReserve[client] = currentReserve;
			
			p_notGunAllowed[client] = true;
			CreateTimer(1.0, t_ResetGunAllowed, client);
		} else
			CreateGunAndAmmo(client, "weapon_awp", 1, 1, 6);
	}
}

public Action t_ResetGunAllowed(Handle timer, int client)
{
	p_notGunAllowed[client] = false;
}

/* Perks */
public void ErrorMsg(int client)
{
	PrintToChat(client, "%s You already posses all Passive Perks", PLUGIN_PREFIX);
}

public void p_deathNade(int client)
{
	PrintToChat(client, "%s \x0BPassive:\x01 You picked up a \x10Death Nade \x01untill \x07Death!", PLUGIN_PREFIX);
	g_pPassivePerks[client][dropExplosion] = true;
}

public void p_hasSniper(int client)
{
	PrintToChat(client, "%s \x0BPassive:\x01 You picked up a \x10Secondary Awp \x01until \x07Death!", PLUGIN_PREFIX);
	g_pPassivePerks[client][hasSniper] = true;
}

public void p_FlashRun(int client)
{
	PrintToChat(client, "%s \x0BActive:\x01 You picked up \x10Flash Run \x01for \x0710 Seconds!", PLUGIN_PREFIX);
	g_pActivePerks[client][flashRun] = true;
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 2.0);
	
	DataPack pack = new DataPack();
	pack.WriteCell(client);
	pack.WriteCell(flashRun);
	
	g_pActivePerksReset[client][0] = CreateTimer(10.0, le10Up, pack);
}

public void p_LightRun(int client)
{
	PrintToChat(client, "%s \x0BActive:\x01 You picked up \x10Light Run \x01until \x07Death!", PLUGIN_PREFIX);
	g_pActivePerks[client][lightRun] = true;
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.25);
}

public void p_DoubleDamage(int client)
{
	PrintToChat(client, "%s \x0BActive:\x01 You picked up \x10Double Damage \x01for \x0710 Seconds!", PLUGIN_PREFIX);
	g_pActivePerks[client][doubleDamage] = true;
	
	DataPack pack = new DataPack();
	pack.WriteCell(client);
	pack.WriteCell(doubleDamage);
	
	g_pActivePerksReset[client][2] = CreateTimer(10.0, le10Up, pack);
}

public void p_StoppingDamage(int client)
{
	PrintToChat(client, "%s \x0BActive:\x01 You picked up \x10Stopping Power \x01until \x07Death!", PLUGIN_PREFIX);
	g_pActivePerks[client][stoppingDamage] = true;
}

public void p_DoubleRegen(int client)
{
	PrintToChat(client, "%s \x0BActive:\x01 You picked up \x10Double Regen \x01until \x07Death!", PLUGIN_PREFIX);
	g_pActivePerks[client][doubleRegen] = true;
}

public Action le10Up(Handle timer, DataPack pack)
{
	pack.Reset();
	int client = pack.ReadCell();
	int perkID = pack.ReadCell();
	
	switch (perkID)
	{
		case 0:
		PrintToChat(client, "%s Your \x10Flash Run\x01 has faded!", PLUGIN_PREFIX);
		case 2:
		PrintToChat(client, "%s Your \x10Double Damage\x01 has faded!", PLUGIN_PREFIX);
	}
	
	g_pActivePerks[client][perkID] = false;
	g_pActivePerksReset[client][perkID] = INVALID_HANDLE;
	
	if (perkID == 0)
		ResetSpeed(client);
}

public void ResetSpeed(int client)
{
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.0);
	if (g_pActivePerksReset[client][2] != INVALID_HANDLE)
	{
		KillTimer(g_pActivePerksReset[client][2]);
		g_pActivePerksReset[client][2] = INVALID_HANDLE;
	}
}

stock CreateGunAndAmmo(int client, char[] name, int clip, int ammo, int ammoType)
{
	int weaponIndex = GivePlayerItem(client, name);
	SetEntProp(weaponIndex, Prop_Send, "m_iClip1", clip);
	SetEntProp(weaponIndex, Prop_Send, "m_iPrimaryReserveAmmoCount", ammo);
	SetEntProp(client, Prop_Send, "m_iAmmo", ammo, _, ammoType);
}

public void ResetPreviousWeapon(client)
{
	p_GunPreviousName[client] = "";
	p_GunPreviousClip[client] = 0;
	p_GunPreviousReserve[client] = 0;
	p_GunPreviousAmmoType[client] = 0;
}

/* STOCKS */
stock GivePlayerNewWeapon(int client, int slot, char[] name)
{
	char buffer[512];
	
	int clientWeapon = GetPlayerWeaponSlot(client, slot);
	if (clientWeapon != -1)
	{
		RemovePlayerItem(client, clientWeapon);
		RemoveEdict(clientWeapon);
	}
	
	GivePlayerItem(client, name);
	Format(buffer, sizeof(buffer), "%s", name);
	
	if (slot == 0)
	{
		p_CurrentLoadOut[client][gunPrimary] = buffer;
		RenameWeapon(name, buffer, sizeof(buffer));
		PrintToChat(client, "%s Your primary is now: \x10%s", PLUGIN_PREFIX, buffer);
	} else if (slot == 1) {
		p_CurrentLoadOut[client][gunSecondary] = buffer;
		RenameWeapon(name, buffer, sizeof(buffer));
		PrintToChat(client, "%s Your secondary is now: \x10%s", PLUGIN_PREFIX, buffer);
	}
}

stock RenameWeapon(char[] input, char[] name, maxlength)
{
	if (StrEqual(input, "weapon_nova"))
		Format(name, maxlength, "Nova");
	else if (StrEqual(input, "weapon_xm1014"))
		Format(name, maxlength, "XM1014");
	else if (StrEqual(input, "weapon_sawedoff"))
		Format(name, maxlength, "Sawed-Off");
	else if (StrEqual(input, "weapon_mag7"))
		Format(name, maxlength, "Mag-7");
	else if (StrEqual(input, "weapon_m249"))
		Format(name, maxlength, "M249");
	else if (StrEqual(input, "weapon_negev"))
		Format(name, maxlength, "Negev");
	else if (StrEqual(input, "weapon_mac10"))
		Format(name, maxlength, "MAC-10");
	else if (StrEqual(input, "weapon_mp7"))
		Format(name, maxlength, "MP7");
	else if (StrEqual(input, "weapon_mp9"))
		Format(name, maxlength, "MP9");
	else if (StrEqual(input, "weapon_bizon"))
		Format(name, maxlength, "PP-Bizon");
	else if (StrEqual(input, "weapon_p90"))
		Format(name, maxlength, "P90");
	else if (StrEqual(input, "weapon_ump45"))
		Format(name, maxlength, "UMP-45");
	else if (StrEqual(input, "weapon_ak47"))
		Format(name, maxlength, "AK-47");
	else if (StrEqual(input, "weapon_aug"))
		Format(name, maxlength, "AUG");
	else if (StrEqual(input, "weapon_famas"))
		Format(name, maxlength, "FAMAS");
	else if (StrEqual(input, "weapon_galilar"))
		Format(name, maxlength, "Galil AR");
	else if (StrEqual(input, "weapon_m4a1_silencer"))
		Format(name, maxlength, "M4A1-S");
	else if (StrEqual(input, "weapon_m4a1"))
		Format(name, maxlength, "M4A4");
	else if (StrEqual(input, "weapon_sg556"))
		Format(name, maxlength, "SG 553");
	else if (StrEqual(input, "weapon_cz75a"))
		Format(name, maxlength, "CZ-75");
	else if (StrEqual(input, "weapon_deagle"))
		Format(name, maxlength, "Desert Eagle");
	else if (StrEqual(input, "weapon_elite"))
		Format(name, maxlength, "Dual Barettas");
	else if (StrEqual(input, "weapon_fiveseven"))
		Format(name, maxlength, "Five-SeveN");
	else if (StrEqual(input, "weapon_glock"))
		Format(name, maxlength, "Glock-18");
	else if (StrEqual(input, "weapon_hkp2000"))
		Format(name, maxlength, "P2000");
	else if (StrEqual(input, "weapon_p250"))
		Format(name, maxlength, "P250");
	else if (StrEqual(input, "weapon_revolver"))
		Format(name, maxlength, "R8 Revolver");
	else if (StrEqual(input, "weapon_tec9"))
		Format(name, maxlength, "Tec-9");
	else if (StrEqual(input, "weapon_usp_silencer"))
		Format(name, maxlength, "USP-S");
} 